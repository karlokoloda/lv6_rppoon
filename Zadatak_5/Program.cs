﻿using System;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);
            logger.Log("Generic message", MessageType.ALL);
            logger.Log("Error message!", MessageType.ERROR | MessageType.WARNING);
        }
    }
}
