﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Zadatak_5
{
    class FileLogger : AbstractLogger
    {
        private string filePath;

        public FileLogger(MessageType messageType, string filePath) : base(messageType) {
            this.filePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type) {
            messageTypeHandling = type;
            File.AppendAllText(filePath, type + ":" + DateTime.Now + "\n" + message + "\n");
        }
    }
}