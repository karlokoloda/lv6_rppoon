﻿using System;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();


            ToDoItem item = new ToDoItem("Item1", "Text1", DateTime.Now);
            careTaker.StoreState(item.StoreState());
            Console.WriteLine(item);

            item.Rename("Title1");
            careTaker.StoreState(item.StoreState());

            item.Rename("Title2");
            Console.WriteLine(item);

            item.RestoreState(careTaker.GetState());
            Console.WriteLine(item);

            item.RestoreState(careTaker.GetState());
            Console.WriteLine(item);

        }
    }
}
