﻿using System;

namespace Zadatak_1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
