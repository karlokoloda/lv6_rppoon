﻿using System;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            Iterator iterator = new Iterator(notebook);

            notebook.AddNote(new Note("Meeting notes", "An email was enough"));
            notebook.AddNote(new Note("Shopping list", "..."));
            notebook.AddNote(new Note("Pay bills", "Electric, internet, gas"));

            while(iterator.IsDone == false) {
                Note note = iterator.First();
                note.Show();
                note = iterator.Next();
            }
        }
    }
}
