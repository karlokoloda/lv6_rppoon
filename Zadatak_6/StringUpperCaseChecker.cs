﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_6
{
    class StringUpperCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool checkUpperCase = false;
            foreach (char letter in stringToCheck) {
                if (char.IsUpper(letter)) checkUpperCase = true;
            }
            return checkUpperCase;
        }
    }
}