﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_6
{
    class StringLengthChecker : StringChecker
    {
        public int checkLength { get; private set; }

        public StringLengthChecker(int MinLength) {
            this.checkLength = MinLength;
        }

        protected override bool PerformCheck(string stringToCheck) {
            return stringToCheck.Length >= this.checkLength;
        }
    }
}