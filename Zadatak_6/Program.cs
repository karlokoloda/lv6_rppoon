﻿using System;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker digitCheck = new StringDigitChecker();
            StringChecker lengthCheck = new StringLengthChecker(5);

            StringChecker lowerCaseCheck = new StringLowerCaseChecker();
            StringChecker upperCaseCheck = new StringUpperCaseChecker();
            
            lowerCaseCheck.SetNext(upperCaseCheck);
            upperCaseCheck.SetNext(lengthCheck);
            lengthCheck.SetNext(digitCheck);

            Console.WriteLine(lowerCaseCheck.Check("i aM sTr1Ng") + "\n");
            Console.WriteLine(upperCaseCheck.Check("i aM sTr1Ng") + "\n");
            Console.WriteLine(digitCheck.Check("i aM sTr1Ng"));
        }
    }
}
