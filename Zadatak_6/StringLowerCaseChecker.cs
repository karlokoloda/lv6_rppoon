﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_6
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool checkLowerCase = false;
            foreach(char letter in stringToCheck) {
                if (char.IsLower(letter)) checkLowerCase = true;
            }
            return checkLowerCase;
        }
    }
}
