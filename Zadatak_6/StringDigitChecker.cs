﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_6
{
    class StringDigitChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool checkDigit = false;
            foreach (char letter in stringToCheck) {
                if (char.IsDigit(letter)) checkDigit = true;
            }
            return checkDigit;
        }
    }
}
