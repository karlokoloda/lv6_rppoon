﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_4
{
    class Memento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }
        public Memento(string ownerName, string ownerAddress, decimal balance) {
            this.OwnerName = ownerName;
            this.OwnerAddress = ownerAddress;
            this.Balance = balance;
        }
    }
}