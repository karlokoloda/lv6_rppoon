﻿using System;

namespace Zadatak_4
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();


            BankAccount bankAccount = new BankAccount("Name", "Adress", decimal.MinusOne);
            careTaker.StoreState(bankAccount.StoreState());
            Console.WriteLine(bankAccount);

            bankAccount.ChangeOwnerAddress("Address1");
            bankAccount.UpdateBalance(100);
            careTaker.StoreState(bankAccount.StoreState());

            bankAccount.ChangeOwnerAddress("Address2");
            bankAccount.UpdateBalance(500);
            Console.WriteLine(bankAccount);

            bankAccount.RestoreState(careTaker.GetState());
            Console.WriteLine(bankAccount);

            bankAccount.RestoreState(careTaker.GetState());
            Console.WriteLine(bankAccount);
        }
    }
}
