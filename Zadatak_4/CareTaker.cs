﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_4
{
    class CareTaker
    {
        private List<Memento> states = new List<Memento>();
        public void StoreState(Memento state) {
            states.Add(state);
        }

        public Memento GetState() {
            if (states.Count == 0) {
                Console.WriteLine("No previous entry!");
                return null;
            }
            else {
                Memento output = states.Last();
                states.Remove(states.Last());
                return output;
            }
        }
    }
}
