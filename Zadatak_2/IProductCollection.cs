﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_2
{
    interface IProductCollection
    {
        IProductIterator GetIterator();
    }
}
