﻿using System;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Milk", 5.49));
            box.AddProduct(new Product("Sugar", 3.99));
            box.AddProduct(new Product("Honey", 19.99));

            Iterator iterator = new Iterator(box);

            while (iterator.IsDone == false)
            {
                Product product = iterator.First();
                Console.WriteLine(product.ToString()+ "\n");
                product = iterator.Next();
            }
        }
    }
}
