﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak_2
{
    interface IProductIterator
    {
        Product First();
        Product Next();
        bool IsDone { get; }
        Product Current { get; }
    }
}
